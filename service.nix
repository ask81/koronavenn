
{config, lib, pkgs, ...}:
let
	koronavenn_build = pkgs.callPackage ./default.nix {};
    	cfg = config.services.koronavenn;
    	
in {
    options.services.koronavenn.enable = lib.mkEnableOption "koronavenn";

    options.services.koronavenn.port = lib.mkOption {
        type = lib.types.int;
        default = 4000;
    };

    config = lib.mkIf cfg.enable {

    networking.firewall.allowedTCPPorts = [ cfg.port ];


  systemd.services.koronavenn = {
        description = "koronavenn";
        environment = {
            HOME="/root";
            PORT="${toString cfg.port}";
            RELEASE_TMP="/tmp";
            };


     after = [ "admin-pwd-key.service" "network-online.target" ];
     wantedBy = ["admin-pwd-key.service" "network-online.target" ];


    serviceConfig = {
        DynamicUser = "true";
        PrivateDevices = "true";
        ProtectKernelTunables = "true";
        ProtectKernelModules = "true";
        ProtectControlGroups = "true";
        RestrictAddressFamilies = "AF_INET AF_INET6";
        LockPersonality = "true";
        RestrictRealtime = "true";
        SystemCallFilter = "@system-service @network-io @signal";
        SystemCallErrorNumber = "EPERM";
        ExecStart = "${koronavenn_build}/bin/koronavenn start";
        Restart = "always";
        RestartSec = "5";
    };
};
};
}
   

#     systemd.services.koronavenn= {
#         description = "koronavenn";
#         after = ["admin-pwd-key.service" "network-online.target" ];
#     	wantedBy = ["admin-pwd-key.service" "network-online.target" ];
# 
#     	script = ''export HOME=/root
#     		   export PORT=${toString cfg.port}
#     		   export RELEASE_TMP=/tmp
#     		   export Restart=on-failure
#     		   ${koronavenn_build}/bin/koronavenn start
#     		   '';
# 
# 
# 
#     };
# };
