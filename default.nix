with import <nixpkgs> {};

    # mix local.rebar --force
#    mix local.hex
let

    # buildNodejs = pkgs.callPackage <nixpkgs/pkgs/development/web/nodejs/nodejs.nix> {};
    
    # nodejs-8 = buildNodejs {
    #   enableNpm = true;
    #   version = "8.17.0";
    #   sha256 = "1zzn7s9wpz1cr4vzrr8n6l1mvg6gdvcfm6f24h1ky9rb93drc3av";
    # };

koronavenn = {}:
 stdenv.mkDerivation rec {
  name = "koronavenn";
  src = ./.;
  buildInputs = [elixir git nodejs];


  buildPhase = ''
  mkdir -p $PWD/.hex
  export HOME=$PWD/.hex
  export GIT_SSL_CAINFO=/etc/ssl/certs/ca-certificates.crt
  export SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
  mix local.rebar --force
  mix local.hex --force
  mix deps.get
  MIX_ENV=prod mix compile
  npm install --prefix ./assets
  npm run deploy --prefix ./assets
  mix phx.digest
  MIX_ENV=prod mix release

  '';
  installPhase = ''
    mkdir -p $out
    cp -rf _build/prod/rel/koronavenn/* $out/

  '';
};
in
koronavenn

