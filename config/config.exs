# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config



signing_salt = :crypto.strong_rand_bytes(32) |> Base.encode64 |> binary_part(0, 32)

config :koronavenn, KoronavennWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: KoronavennWeb.ErrorView, accepts: ~w(html json)],
  pubsub_server: Koronavenn.PubSub,
  code_reloader: false,
  live_view: [
    signing_salt: signing_salt
  ]



# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# config :libcluster,
#   #debug: true,
#   topologies: [
#     gossip_example: [
#       strategy: Elixir.Cluster.Strategy.Gossip,
#       config: [
#         port: 45892,
#         if_addr: "0.0.0.0",
#         multicast_if: "192.168.1.1",
#         multicast_addr: "230.1.1.251",
#         multicast_ttl: 1,
#         secret: "somepassword"]]]



# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
