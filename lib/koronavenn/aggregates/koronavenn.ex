defmodule Koronavenn.KoronavennAggregate do
  defmodule State,
    do: defstruct([:tlf, :name, last_caller_registration: nil, last_receiver_registration: nil])

  defmodule AddCaller, do: defstruct([:ag_id, :name, :country_code, :tlf, :language])
  defmodule AddReceiver, do: defstruct([:ag_id, :name, :country_code, :tlf, :language])

  defmodule CallerAdded,
    do: defstruct([:ag_id, :name, :tlf, :country_code, :event_id, :timestamp, :language])

  defmodule ReceiverAdded,
    do: defstruct([:ag_id, :name, :tlf, :country_code, :event_id, :timestamp, :language])

  def dispatch(%AddReceiver{} = cmd),
    do: SimpleCqrs.Aggregate.dispatch(cmd.ag_id, __MODULE__, cmd)

  def dispatch(%AddCaller{} = cmd),
    do: SimpleCqrs.Aggregate.dispatch(cmd.ag_id, __MODULE__, cmd)

  def dispatch(_), do: raise("Command not implemented for aggregate")

  # Callbacks
  def execute(state, %AddReceiver{} = cmd) do
    time_now = DateTime.now!("Etc/UTC") |> DateTime.to_unix()
    # Validate found number
    case state.last_receiver_registration do
      x when time_now - x < 604_800 ->
        {:error, "Can only register as a receiver once per week"}
      _ ->
        {:ok,
         %ReceiverAdded{
           timestamp: time_now,
           ag_id: cmd.ag_id,
           name: cmd.name,
           tlf: cmd.tlf,
           language: cmd.language,
           country_code: cmd.country_code,
           event_id: UUID.uuid1()
         }}
    end
  end
  # Callbacks
  def execute(state, %AddCaller{} = cmd) do
    time_now = DateTime.now!("Etc/UTC") |> DateTime.to_unix()
    # Validate found number
    case state.last_caller_registration do
      x when time_now - x < 604_800 ->
        {:error, "Can only register as a receiver once per week"}
      _ ->
        {:ok,
         %CallerAdded{
           timestamp: time_now,
           ag_id: cmd.ag_id,
           name: cmd.name,
           tlf: cmd.tlf,
           language: cmd.language,
           country_code: cmd.country_code,
           event_id: UUID.uuid1()
         }}
    end
  end

  def apply_event(_state, %ReceiverAdded{} = event), do: %State{tlf: event.tlf, name: event.name, last_receiver_registration: event.timestamp}
  def apply_event(_state, %CallerAdded{} = event), do: %State{tlf: event.tlf, name: event.name, last_caller_registration: event.timestamp}


end
