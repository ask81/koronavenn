defmodule Koronavenn.MixProject do
  use Mix.Project

  def project do
    [
      app: :koronavenn,
      version: "0.0.1",
      elixir: "~> 1.10.1",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: [
      koronavenn: [
        include_executables_for: [:unix],
        applications: [runtime_tools: :permanent],
        cookie: "koronavenn"
      ]
    ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Koronavenn.Application, []},
      extra_applications: [:logger, :runtime_tools, :distributed_cqrs]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
#      {:phoenix_live_view, "~> 0.9.0", override: true},
  defp deps do
    [
      {:phoenix, "~> 1.5.4"},
      {:phoenix_live_view, "~> 0.14.4"},
      {:floki, ">= 0.0.0", only: :test},
      {:libcluster, "~> 3.2"},
      {:distributed_cqrs, git: "https://gitlab.com/akselsk/simple_cqrs"},
      {:phoenix_pubsub, "~> 2.0"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2.4", only: :dev},
      {:phoenix_live_dashboard, "~> 0.2.7"},
      {:plug_cowboy, "~> 2.0"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
     {:observer_cli, "~> 1.5"}
    ]
  end
end
