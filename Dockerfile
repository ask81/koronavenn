
FROM elixir:1.10.2-alpine AS phx-builder

ENV MIX_ENV=prod

WORKDIR /app

RUN mix local.hex --force
RUN mix local.rebar --force

RUN apk update && \
	apk --no-cache --update add \
	make \
	git \
	curl \
	g++ \
	wget \
	nodejs \
	nodejs-npm && \
	npm install npm -g --no progress

ENV PATH=./node_modules.bin:$PATH

ADD mix.exs mix.lock ./
RUN mix do deps.get, deps.compile

ADD . .

RUN cd assets/ && \
    npm install && \
    npm run deploy && \
    cd - && \
    mix do compile, phx.digest

RUN mix release

FROM elixir:1.10.2-alpine

ENV LANG=C.UTF-8
EXPOSE 5000
ENV PORT=5000 MIX_ENV=prod
RUN apk add --no-cache openssl
RUN apk add --no-cache ncurses-libs
RUN apk add --no-cache bash

COPY --from=phx-builder /app/_build/prod /app/_build/prod

WORKDIR /app/_build/prod/rel/koronavenn/bin/

CMD ["/app/_build/prod/rel/koronavenn/bin/koronavenn", "start"]

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=2 \
 CMD nc -vz -w 2 localhost 4000 || exit 1

