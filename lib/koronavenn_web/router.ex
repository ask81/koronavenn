defmodule KoronavennWeb.Router do
 use KoronavennWeb, :router
   import Phoenix.LiveView.Router
  import Phoenix.LiveDashboard.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :put_root_layout, {KoronavennWeb.LayoutView, "root.html"}
    end

  pipeline :api do
    plug :accepts, ["json"]
  end

 # scope "/" do
  # live_dashboard "/dashboard"
   #end

  scope "/", KoronavennWeb do
    pipe_through :browser
    live_dashboard "/dashboard"
    live "/", App
    end

  # Other scopes may use custom stacks.
  # scope "/api", KoronavennWeb do
  #   pipe_through :api
  # end
end
