defmodule KoronavennWeb.App do
  use Phoenix.LiveView

  @moduledoc """
  """
  @doc """
  Renders the initial html and sets up the liveview WS connection
  """

  def mount(_params, session, socket) do
    {:ok, assign(socket, :flash_msg, nil)}
  end


  def render(assigns) do
    ~L"""

    <div class="notif  margin-top-10">
    <h3> Ettersom Norge har løsnet
    på tiltakene ser vi ikke lenger behovet
    for å drifte denne siden.
    </br>

    Man vil derfor ikke lenger få
    en koronavenn om man melder seg på.
    </br>

    Vi håper imidlertid at de som har fått en kornavenn
    holder kontaken.
    </br>
    Takk for at du bidro!
</h3>
    </div>


    <div class="row">
    <div class=" column left-color screen-height padding-sides ">
    <div class="container left-container">



    <div class="left-header">
    						<h1> Koronavenn </h1>
          <p class="header"> En samtalepartner kan gjøre vanskelige tider enklere å håndtere      </p>
    </div>


    <p class="content">
    Trenger du noen å snakke med?  Eller har du et ønske om å hjelpe noen ved å gi litt av din tid og ditt overskudd gjennom en telefonsamtale? En koronavenn kan bringe trygghet og glede inn i livet til mennesker som føler seg ensomme, bekymret og ønsker noen å snakke med.
    </p>
    <ol class="bullet-points">
<li> Registrer deg som koronavenn </li>
<li> Motta en tekstmelding med navn og telefonnr til din koronavenn </li>
<li> Ring hverandre  <3  </li>
<li> Dere bestemmer selv hvordan den videre kontakten skal foregå </li>
   </ol>


    <p class="footer">
    Tjenesten koronavenn.no er organisert på frivillig basis for å gjøre en vanskelig tid litt mindre ensom for de som trenger det. Det er ikke en helsetjeneste, og kan dermed ikke erstatte helsehjelp gitt av utdannet helsepersonell. Personer du kommer i kontakt med er ikke underlagt taushetsplikt i forbindelse med bruk av tjenesten. For henvendelser kontakt oss på koronavenn@gmail.com. 
    </p>

    </div>
    </div>

    <div class="column left-color screen-height middle-container">
    <div class="left-header">
    </div>
     <p class="header middle-container">
     Fem tips til den gode samtalen
     </p>
     <ol class="middle-container bullet-points ">
     <li> Lytting er godt nok. Ikke alle vanskeligheter løses så lett, og man kommer langt med forståelse. </li>
     <li>  Still åpne spørsmål som gir den andre mulighet til å fortelle på sin måte. </li>
     <li> Små oppsummeringer av det den andre har sagt, sikrer din egen forståelse og viser at du ønsker å forstå. </li>
     <li> Verken stillhet, tårer eller usikkerhet er farlig. Det er plass til stillhet i en god samtale. Om du ikke vet hva du skal si - si det. </li>
     <li> Du er god nok som du er. Det trenger ikke være vanskelig - ofte er en helt hverdagslig prat akkurat det som trengs. Og er du usikker - spør!
     </li>
     </ol>
     <div class="telefon-img telefon-container">
      <img width="100%" src="<%= KoronavennWeb.Router.Helpers.static_path(KoronavennWeb.Endpoint, "/images/telefon.svg") %>">
     </div>
    </div>

    <div class="column right-color screen-height padding-sides">

    <div class="container padding-sides-small padding-top-5">

    <%= if @flash_msg != nil do %>
    <div class="alert alert-danger"> <%= @flash_msg %> </div>
    <% end %>

    <form class="form padding-top-5" action="https://docs.google.com/forms/u/0/d/e/1FAIpQLSckawYxQHQeTf1FufvArqz3gDZFmyH-0lzrMHebiS3JqwJhTg/formResponse">
     <p class="form-title"> Bli en Korona-venn </p>
         <input name="entry.1533655926" Placeholder="Fornavn" type="text" />
         <input name="entry.593474454" Placeholder="Telefon" type="tel" required />
         </br>
         </br>


    <label class="checkbox-label" for="male">
    <input type="radio" id="male" name="entry.1812359444" value="Caller">
    Jeg ønsker å ringe noen som trenger det
    </label>
    <label class="checkbox-label" for="female">
    <input type="radio" id="female" name="entry.1812359444" value="Receiver">
    Jeg ønsker noen å snakke med og vil bli ringt
    </label>
    <input class="submit-button" type="submit" value="Send" />
    </form>
    <p class="drive-rules-title"> Kjøreregler for en trygg relasjon</p>
    <ol class="drive-rules-bullet-points">
    <li> Avklar behovet. Er det å få lufte tanker, å snakke om løst og fast, eller å få råd til en konkret utfordring?</li>
    <li> Avklar forventninger. Hvor hyppig kontakt ønsker dere? Hvor mye tid skal vies til dette?</li>
    <li> Grensesetting gir trygghet. Du verken kan eller skal være alt for den andre. </li>
    <li> Husk at du ikke er en fagperson. Ved krevende problemstillinger/situasjoner skal du vise til profesjonelle tjenester.</li>
    </ol>
    <p class="copyright">
    På initiativ fra Uromaker AS </br>
    Design og kode av Aksel Stadler Kjetså og Nora Markussen </p>
    </div>
    </div>

    </div>
    """
  end
end

#    <h2> Bli en Koronna-venn </h2>
# <form phx-submit="save">
#     <label for="nameField">Fornavn</label>
#     <input name="fornavn" type="text" id="nameField">
#     <label for="telefon">Nummer</label>
#     <input name="tlf" type="text" id="telefon">

#         <label for="ringe"  style="word-wrap:break-word">
#       <input type="checkbox" id="ringe" name="ringe" checked> Jeg vil ringe!
#       </label>

#         <label for="bli ringt"  style="word-wrap:break-word">
#       <input type="checkbox" id="bli ringt" name="bli ringt" checked> Jeg vil bli ringt!
#       </label>

#     <input class="button-primary" type="submit" value="Bli en Koronna-venn">

# </form>
